from django import forms
from .models import Game, Player
from django_comments.forms import CommentForm


class GameForm(forms.ModelForm):

    class Meta:
        model = Game
        fields = ('place_coordinatesX', 'place_coordinatesY', 'place_name', 'description')


class PlayerForm(forms.ModelForm):

    class Meta:
        model = Player
        fields = ('user', 'image')


