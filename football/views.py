from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from .forms import GameForm, PlayerForm
from .models import Game, Player


# Create your views here.
def user_login(request):

    # Если запрос HTTP POST, пытаемся извлечь нужную информацию.
    if request.method == 'POST':
        # Получаем имя пользователя и пароль, вводимые пользователем.
        # Эта информация извлекается из формы входа в систему.
                # Мы используем request.POST.get('<имя переменной>') вместо request.POST['<имя переменной>'],
                # потому что request.POST.get('<имя переменной>') вернет None, если значения не существует,
                # тогда как request.POST['<variable>'] создаст исключение, связанное с отсутствем значения с таким ключом
        username = request.POST.get('username')
        password = request.POST.get('password')

        # Используйте Django, чтобы проверить является ли правильным
        # сочетание имя пользователя/пароль - если да, то возвращается объект User.
        user = authenticate(username=username, password=password)

        # Если мы получили объект User, то данные верны.
        # Если получено None (так Python представляет отсутствие значения), то пользователь
        # с такими учетными данными не был найден.
        if user:
            # Аккаунт активен? Он может быть отключен.
            if user.is_active:
                # Если учетные данные верны и аккаунт активен, мы можем позволить пользователю войти в систему.
                # Мы возвращаем его обратно на главную страницу.
                login(request, user)
                return HttpResponseRedirect(reverse(list))
            else:
                # Использовался не активный аккуант - запретить вход!
                return HttpResponse("Your FOOTBALL account is disabled.")
        else:
            # Были введены неверные данные для входа. Из-за этого вход в систему не возможен.
            # print("Invalid login details: {0}, {1}".format(username, password))
            return HttpResponse("Invalid login details supplied.")

    # Запрос не HTTP POST, поэтому выводим форму для входа в систему.
    # В этом случае скорее всего использовался HTTP GET запрос.
    else:
        # Ни одна переменная контекста не передается в систему шаблонов, следовательно, используется
        # объект пустого словаря...
        return render(request, 'login.html', {})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('list'))


def game_new(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            form = GameForm(request.POST)
            form.date = timezone.now()
            game = form.save(commit=False)
            game.save()
            game_create = 'game create'
            return render(request, 'game_new.html', {'game_create': game_create})
        else:
            form = GameForm()
            return render(request, 'game_new.html', {'form': form})
    else:
        return HttpResponseRedirect(reverse('login'))


def player_new(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            form = PlayerForm(request.POST)
            form.date = timezone.now()
            player = form.save(commit=False)
            player.save()
            player_create = 'player create'
            return render(request, 'player_new.html', {'player_create': player_create})
        else:
            form = PlayerForm()
            return render(request, 'player_new.html', {'form': form})
    else:
        return HttpResponseRedirect(reverse('login'))


def list(request):
    games = Game.objects.all()
    players = Player.objects.all()
    ctx = {
        'games': games,
        'players': players
    }
    return render(request, 'football.html', ctx)
