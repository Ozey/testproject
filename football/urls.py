from django.conf.urls import include, url
from django.contrib.auth.views import logout

from . import views

urlpatterns = [
    # url(r'^$', views.post_list, name='post_list'),
    url(r'^login/$', views.user_login, name='login'),
    # url('^', include('django.contrib.auth.urls')),
    url(r'^game/new/$', views.game_new, name='game_new'),
    url(r'^player/new/$', views.player_new, name='player_new'),
    url(r'^$', views.list, name='list'),
    # url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^logout/$', logout, {'next_page': 'list'}, name='logout'),
    url(r'^comments/', include('django_comments.urls')),



]