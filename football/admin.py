from django.contrib import admin

# Register your models here.
from .models import Game, Player, Rating

admin.site.register(Game)
admin.site.register(Player)
admin.site.register(Rating)


