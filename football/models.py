from django.db import models
from django.contrib.auth.models import User
from django_comments.models import Comment
from django.core.urlresolvers import reverse
# Create your models here.


class Game(models.Model):

    date = models.DateTimeField(null=True)
    place_coordinatesX = models.FloatField(null=True)
    place_coordinatesY = models.FloatField(null=True)
    place_name = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self):
        return self.description +" "+str(self.date)


class Player(models.Model):

    user = models.OneToOneField(User, null=True)
    image = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.user.first_name


class Rating (models.Model):

    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    user_give_rating = models.ForeignKey(Player, on_delete=models.CASCADE)  # пользователь ставит рейтинг
    user_receive_rating = models.ForeignKey(
        Player, on_delete=models.CASCADE,
        related_name='user_receive_rating')   # пользователю ставят рейтинг
    time = models.DateTimeField(auto_now_add=True)
    rating = models.IntegerField(default=0, null=True)

    def __str__(self):
        return self.rating
