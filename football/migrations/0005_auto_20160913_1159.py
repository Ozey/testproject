# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-13 11:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('football', '0004_auto_20160913_0941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
